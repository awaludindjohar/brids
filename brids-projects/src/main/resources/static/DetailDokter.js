
$(document).ready(function() {
	biodatadokter();
	function biodatadokter() {

		var id = sessionStorage.getItem("id");
		$.ajax({
			url: "http://localhost/apidoctor/doctor/" + id,
			type: "GET",
			success: function(hasil) {
				$(".namadokter").html(hasil.fullname);
				$(".img").attr("src", hasil.image_path + hasil.image);
				$(".spesialis").html(hasil.spesialis);
			}
		});

	};

	pengalaman();
	function pengalaman() {
		var id = sessionStorage.getItem("id");
		$.ajax({
			url: "http://localhost/apidoctor/pengalaman/" + id,
			type: "GET",
			success: function(hasil) {
				$(".pengalaman").html(hasil[0].pengalaman + " tahun pengalaman");
			}
		});
	}
	
	chat();
	function chat() {
		var id = sessionStorage.getItem("id");
		$.ajax({
			url: "http://localhost/apidoctor/chat",
			type: "GET",
			success: function(hasil) {
				$(".chat").html("Rp. "+hasil[0].start_from);
			}
		});
	}

	tindakanmedis();
	function tindakanmedis() {

		var id = sessionStorage.getItem("id");
		$.ajax({
			url: "http://localhost/apidoctor/tindakanmedis/" + id,
			type: "GET",
			success: function(hasil) {
				var tnd = ""
				for (i = 0; i < hasil.length; i++) {
					if (hasil[i].tindakan == "chat") {

					} else {
						tnd += "- " + hasil[i].tindakan + "<br>"
					}
				}
				$("#tindakan").html(tnd);
			}
		});

	};

	pendidikandokter();
	function pendidikandokter() {

		var id = sessionStorage.getItem("id");
		$.ajax({
			url: "http://localhost/apidoctor/pendidikandokter/" + id,
			type: "GET",
			success: function(hasil) {
				var tnd = ""
				for (i = 0; i < hasil.length; i++) {
					tnd += "<h6>" + hasil[i].institution_name + "</h6>"
					tnd += "<p style='margin-top: -10px;'><div class='left'>" + hasil[i].major + "</div>"
					tnd += "<div class='right'>" + hasil[i].end_year + "</div></p><br><br>"
				}
				$("#pendidikan").html(tnd);
			}
		});

	};
	var tnd = "";
	var xx = "";
	lokasipraktek();
	function lokasipraktek() {

		var id = sessionStorage.getItem("id");
		$.ajax({
			url: "http://localhost/apidoctor/lokasipraktek/" + id,
			type: "GET",
			async: false,
			success: function(hasil) {
				for (i = 0; i < hasil.length; i++) {
					var jj = "";
					var price = "";
					tnd += "<div class='detailkanan'>"
					tnd += "<h6>" + hasil[i].name + "</h6>"
					tnd += "<div class='row'>"
					tnd += "<div class='col-md-8'>"
					tnd += "<p style='margin-top: -10px;'>" + hasil[i].kategori + "<br>"
					tnd += "<i class='fa-solid fa-location-dot' style='color: gray;'></i> " + hasil[i].full_address + "</p>"
					tnd += "</div>"
					tnd += "<div class='col-md-4'>"

					$.ajax({
						url: "http://localhost/apidoctor/price/" + hasil[i].id,
						type: "GET",
						async: false,
						success: function(hasil) {
							price += "<p class='coco' style='text-align: center;'>Konsultasi mulai dari <br>"
							price += "Rp. " + hasil[0].start_from + "</p>"
						}
					});

					tnd += price;
					tnd += "</div>"
					tnd += "</div>"
					tnd += "<hr style='border: 1px solid #008CBA;'>"

					tnd += "<button id='show" + hasil[i].id + "' style='display:block; text-decoration:none;' class='btn btn-link btn-xs'><i class='fa-solid fa-angle-down'></i> Lihat jadwal praktek</button>"
					tnd += "<button id='hide" + hasil[i].id + "' style='display:none;text-decoration:none;' class='btn btn-link btn-xs'><i class='fa-solid fa-angle-up'></i> Sembunyikan jadwal praktek</button>"


					$.ajax({
						url: "http://localhost/apidoctor/jadwal?id_doctor=" + id + "&medical_facility_id=" + hasil[i].id,
						type: "GET",
						async: false,
						success: function(hasil) {
							jj += "<div id='jadwal" + hasil[0].medical_facility_id + "' style='display:none;'>"
							jj += "<div class='row'>"
							jj += "<div class='col-md-9'>"

							for (j = 0; j < hasil.length; j++) {
								jj += "<div class='row'>"
								jj += "<div class='col-md-3'>"
								jj += "<p>" + hasil[j].day + "</p>"
								jj += "</div>"

								jj += "<div class='col-md-6'>"
								jj += "<p>" + hasil[j].time_schedule_start + " - " + hasil[j].time_schedule_end + "</p>"
								jj += "</div>"
								jj += "</div>"
							}
							jj += "</div>"

							jj += "<div class='col-md-3'>"
							jj += "<button class='button5'>Buat Janji</button>"
							jj += "</div>"

							jj += "</div>"
							jj += "</div>"
						}
					});
					tnd += jj;
					tnd += "</div>"
				}
				$("#lokasi").html(tnd);
			}
		});

	};

	history();
	function history() {

		var id = sessionStorage.getItem("id");
		$.ajax({
			url: "http://localhost/apidoctor/history/" + id,
			type: "GET",
			success: function(hasil) {
				var tnd = ""
				for (i = 0; i < hasil.length; i++) {
					if (hasil[i].end_year == null) {
						end = "sekarang";
					} else {
						end = hasil[i].end_year;
					}

					tnd += "<h6>" + hasil[i].rs_name + ", " + hasil[i].location_name + "</h6>"
					tnd += "<p style='margin-top: -10px;'><div class='left'>" + hasil[i].spesialis + "</div>"
					tnd += "<div class='right'>" + hasil[i].start_year + " - " + end + "</div></p><br><br>"
				}
				$("#history").html(tnd);
			}
		});

	};


	aturjadwal();
	function aturjadwal() {
		var i = 1;
		$("#show" + i).click(function() {
			document.getElementById("jadwal" + i).style.display = "block";
			document.getElementById("show" + i).style.display = "none";
			document.getElementById("hide" + i).style.display = "block";
		});

		$("#hide" + i).click(function() {
			document.getElementById("jadwal" + i).style.display = "none";
			document.getElementById("show" + i).style.display = "block";
			document.getElementById("hide" + i).style.display = "none";
		});

		z = 2;
		$("#show" + z).click(function() {
			document.getElementById("jadwal" + z).style.display = "block";
			document.getElementById("show" + z).style.display = "none";
			document.getElementById("hide" + z).style.display = "block";
		});

		$("#hide" + z).click(function() {
			document.getElementById("jadwal" + z).style.display = "none";
			document.getElementById("show" + z).style.display = "block";
			document.getElementById("hide" + z).style.display = "none";
		});


	}

})
