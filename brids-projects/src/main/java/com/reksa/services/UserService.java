package com.reksa.services;

import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reksa.models.UserModel;
import com.reksa.repositories.UserRepository;

@Service
@Transactional
public class UserService {
	
	@Autowired
	private UserRepository ur;
	
	public Map<String, Object> login(String email, String password) {
		return ur.login(email, password);
	}
	
}
