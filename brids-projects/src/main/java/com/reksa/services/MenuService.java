package com.reksa.services;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reksa.models.MenuModel;
import com.reksa.repositories.MenuRepository;

@Service
@Transactional
public class MenuService {
	
	@Autowired
	private MenuRepository mr;
	
	public List<MenuModel>menu() {
		return mr.menu();
	}		
	
	public List<Map<String, Object>> listId(long id) {
		return mr.listId(id);
	}
	
	public int delMenu(long id) {
		return mr.delMenu(id);
	}
	
	public int addMenu(String name, String url, long parent_id, String big_icon, String small_icon) {
		return mr.addMenu(name, url, parent_id, big_icon, small_icon);
	}
	
	public String cariNama(String name) {
		return mr.cariNama(name);
	}

}
