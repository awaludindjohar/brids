package com.reksa.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.reksa.models.BankModel;
import com.reksa.repositories.BankRepository;

@Service
@Transactional
public class BankService {
	
	@Autowired
	private BankRepository br;
	
	public List<BankModel>bank() {
		return br.bank();
	}
	
	public BankModel listBankById(long id) {
		return br.listBankById(id);
	}
	
	public int addBank(String nama, String va_kode) {
		return br.addBank(nama, va_kode);
	}
	
	public int updBank(long id, String nama, String va_kode) {
		return br.updBank(nama, va_kode, id);
	}
	
	public int delBank(long id) {
		return br.delBank(id);
	}
	
	public List<BankModel> bankSearch(String nama) {
		return br.bankSearch(nama.toLowerCase());
	}
	
	public String cariNama(String nama) {
		return br.cariNama(nama);
	}
	
	public BankModel cariNama2(String nama) {
		return br.cariNama2(nama);
	}
	
	public BankModel cariKodeVa(String va_kode) {
		return br.cariKodeVa(va_kode);
	}

}
