package com.reksa.services;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.reksa.repositories.DetailDoctorRepository;

@Service
@Transactional
public class DetailDokterService {
	
	@Autowired
	private DetailDoctorRepository ddr;
	
	public Map<String, Object> ListDoctor(long id) {
		return ddr.ListDoctor(id);
	}
	
	public List<Map<String, Object>>ListTindakanMedis(long id) {
		return ddr.ListTindakanMedis(id);
	}
	
	public List<Map<String, Object>>ListPendidikanDokter(long id) {
		return ddr.ListPendidikanDokter(id);
	}
	
	public List<Map<String, Object>>ListLokasiPraktek(long id) {
		return ddr.ListLokasiPraktek(id);
	}
	
	public List<Map<String, Object>>ListSchedule(long id) {
		return ddr.ListSchedule(id);
	}
	
	public List<Map<String, Object>>ListJadwal(long id, long mfid) {
		return ddr.ListJadwal(id, mfid);
	}
	
	public List<Map<String, Object>>ListHistory(long id) {
		return ddr.ListHistory(id);
	}
	
	public List<Map<String, Object>>Pengalaman(long id) {
		return ddr.Pengalaman(id);
	}
	
	public List<Map<String, Object>>Price(long id) {
		return ddr.Price(id);
	}
	
	public List<Map<String, Object>> Chat() {
		return ddr.Chat();
	}

}
