package com.reksa.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.reksa.models.BankModel;

public interface BankRepository extends JpaRepository<BankModel, Long>{
	
	@Query(value = " select * from m_bank where is_delete = false"
				 + " order by id asc ", nativeQuery = true)
	List<BankModel> bank();
	
	@Query(value = " select * "
				 + " from m_bank where id = :id", nativeQuery = true)
	BankModel listBankById(long id);
	
	@Modifying
	@Query(value = "insert into m_bank(is_delete, nama, va_kode, created_by, created_on) values" + 
				   "(false, :nama, :va_kode, 1, now())", nativeQuery = true)
	int addBank(String nama, String va_kode);
	
	@Modifying
	@Query(value = " update m_bank set nama=:nama, va_kode=:va_kode, modified_by = 1, modified_on = now()"
				 + " where id=:id ", nativeQuery = true)
	int updBank(String nama, String va_kode, long id);
	
	@Modifying
	@Query(value = " update m_bank set is_delete = true, deleted_by = 1, deleted_on = now()"
				 + " where id=:id ", nativeQuery = true)
	int delBank(long id);
	
	@Modifying
	@Query(value = " select * from m_bank where lower(nama) like '%:nama%' ", nativeQuery = true)
	List<BankModel> bankSearch(String nama);
	
	@Query(value = "select nama from m_bank where nama = :nama", nativeQuery = true)
	String cariNama(String nama);
	
	@Query(value = "select * from m_bank where nama = :nama ", nativeQuery = true)
	BankModel cariNama2(String nama);
	
	@Query(value = "select * from m_bank where va_kode = :va_kode ", nativeQuery = true)
	BankModel cariKodeVa(String va_kode);

}
