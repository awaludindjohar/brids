package com.reksa.repositories;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.reksa.models.DoctorModel;

public interface DetailDoctorRepository extends JpaRepository<DoctorModel, Long>{	
	
	@Query(value = "select * from vdoctor where id_doctor = :id", nativeQuery = true)
	Map<String, Object>ListDoctor(long id);
	
	@Query(value = "select * from vtindakan where id_doctor = :id", nativeQuery = true)
	List<Map<String, Object>>ListTindakanMedis(long id);
	
	@Query(value = "select * from vpendidikan_dokter where id_doctor = :id", nativeQuery = true)
	List<Map<String, Object>>ListPendidikanDokter(long id);
	
	@Query(value = "select * from vlokasi_praktek where id_doctor = :id and is_delete = false", nativeQuery = true)
	List<Map<String, Object>>ListLokasiPraktek(long id);
	
	@Query(value = "select * from vschedule where id_doctor = :id", nativeQuery = true)
	List<Map<String, Object>>ListSchedule(long id);
	
	@Query(value = "select * from vschedule where id_doctor = :id and medical_facility_id = :mfid", nativeQuery = true)
	List<Map<String, Object>>ListJadwal(long id, long mfid);
	
	@Query(value = "select * from vhistory where id_doctor = :id", nativeQuery = true)
	List<Map<String, Object>>ListHistory(long id);
	
	@Query(value = "select * from vpengalaman where id_doctor = :id", nativeQuery = true)
	List<Map<String, Object>>Pengalaman(long id);
	
	@Query(value = "select * from vprice where doctor_office_id = :id", nativeQuery = true)
	List<Map<String, Object>>Price(long id);
	
	@Query(value = "select * from vchat", nativeQuery = true)
	List<Map<String, Object>>Chat();
}
