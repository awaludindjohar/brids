package com.reksa.repositories;

import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.reksa.models.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Long>{
	
	@Query(value = " select fullname,role_id from m_user u join m_biodata b on u.biodata_id=b.id where email=:email "
			+ " and password=:password limit 1 ", nativeQuery = true)
	Map<String, Object> login(String email, String password);
	
}
