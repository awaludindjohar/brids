package com.reksa.repositories;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.reksa.models.MenuModel;

public interface MenuRepository extends JpaRepository<MenuModel, Long>{
	
	@Query(value = " select * from m_menu where is_delete = false ", nativeQuery = true)
	List<MenuModel> menu();
	
	@Query(value = " select * from vrole where role_id = :id ", nativeQuery = true)
	List<Map<String, Object>> listId(long id);
	
	@Modifying
	@Query(value = " update m_menu set is_delete = true, deleted_by = 1, deleted_on = now()"
				 + " where id=:id ", nativeQuery = true)
	int delMenu(long id);
	
	@Modifying
	@Query(value = "insert into m_menu(is_delete,created_by, created_on,name,url,parent_id,big_icon,small_icon) values" + 
				   "(false, 1, now(), :name, :url, :parent_id, :big_icon, :small_icon)", nativeQuery = true)
	int addMenu(String name, String url, long parent_id, String big_icon, String small_icon);
	
	@Query(value = " select name from m_menu where name = :name ", nativeQuery = true)
	String cariNama(String name);
  
}
