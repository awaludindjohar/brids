
package com.reksa.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.reksa.models.UserModel;
import com.reksa.services.UserService;

@Controller
public class DefaultController {

	@Autowired
	private UserService us;

	//
	@RequestMapping(value = "/")
	public String dash() {
		return "/LandingPage";
	}

	@RequestMapping(value = "dashboard")
	public String dashboard(HttpSession session, Model m) {
		Object logEmail = session.getAttribute("logEmail");
		Object lRoleId = session.getAttribute("lRoleId");
		m.addAttribute("hActiveUser", logEmail);
		m.addAttribute("lRoleId", lRoleId);
		return "/Dashboard";
	}

	@RequestMapping("/login")
	public String login() {

		return "Login";
	}

	@RequestMapping("signin")
	public String signin(@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "password", required = false) String password, HttpServletRequest r) {
		String url = "";
		Map<String, Object> um = us.login(email, password);
		if (um != null) {
			r.getSession().setAttribute("logEmail", um.get("fullname"));
			r.getSession().setAttribute("lRoleId", um.get("role_id"));
			url = "redirect:/dashboard";
		} else {
			url = "/LandingPage";
		}
		return url;
	}

	@RequestMapping("/logout")
	public String logout(HttpServletRequest h) {
		h.getSession().invalidate();
		return "redirect:/LandingPage";
	}
	
	@RequestMapping(value = "/caridokter")
	public String dash2() {
		return "/CariDokter";
	}
	
	@RequestMapping(value = "/detaildokter")
	public String dash3() {
		return "/DetailDokter";
	}

}
