package com.reksa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reksa.models.BankModel;
import com.reksa.services.BankService;

@RestController
@RequestMapping("apibank")
public class BankRestController {
	
	@Autowired
	private BankService bs;
	
	@GetMapping("bank") 
	public List<BankModel> bank() {
		return bs.bank();
	}
	
	@GetMapping("bank/{id}")
	public BankModel listBankById(@PathVariable long id) {
		return bs.listBankById(id);
	}
	
	@PostMapping("bank")
	public String addBank(@RequestBody BankModel bm) {
		String nama = bs.cariNama(bm.getNama());
		if(nama == null) {
			try {
				int hasil = bs.addBank(bm.getNama(), bm.getVaKode());
			} catch (Exception e) {
				return "Tambah Gagal, id Sudah di Pakai";
			}
		} else {
			return "Nama sudah di pakai";
		}
		return "Tambah Sukses";
	}
	
	@PutMapping("bank")
	public int updBank(@RequestBody BankModel bm) {
		return bs.updBank(bm.getId(), bm.getNama(), bm.getVaKode());
	}
	
	@DeleteMapping("bank/{id}")
	public String delBank(@PathVariable int id) {
		int hasil = bs.delBank(id);
		if(hasil == 1) {
			return "Delete Sukses";
		} else {
			return "Delete Gagal";
		}
	}
	
	@GetMapping("namabank/{nama}")
	public BankModel cariNama2(@PathVariable String nama){
		return bs.cariNama2(nama);
	}
	
	@GetMapping("kodebank/{va_kode}")
	public BankModel cariKodeVa(@PathVariable String va_kode){
		return bs.cariKodeVa(va_kode);
	}

}
