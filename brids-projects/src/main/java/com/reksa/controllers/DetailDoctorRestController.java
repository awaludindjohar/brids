package com.reksa.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.reksa.services.DetailDokterService;

@RestController
@RequestMapping("apidoctor")
public class DetailDoctorRestController {
	
	@Autowired
	private DetailDokterService dds;
	
	@GetMapping("doctor/{id}")
	public Map<String, Object> ListDoctor(@PathVariable long id){
		return dds.ListDoctor(id);
	}
	
	@GetMapping("tindakanmedis/{id}")
	public List<Map<String, Object>> ListTindakanMedis(@PathVariable long id){
		return dds.ListTindakanMedis(id);
	}
	
	@GetMapping("pendidikandokter/{id}")
	public List<Map<String, Object>> ListPendidikanDokter(@PathVariable long id){
		return dds.ListPendidikanDokter(id);
	}
	
	@GetMapping("lokasipraktek/{id}")
	public List<Map<String, Object>> ListLokasiPraktek(@PathVariable long id){
		return dds.ListLokasiPraktek(id);
	}
	
	@GetMapping("schedule/{id}")
	public List<Map<String, Object>> ListSchedule(@PathVariable long id){
		return dds.ListSchedule(id);
	}
	
	@GetMapping("jadwal")
	public List<Map<String, Object>> ListJadwal(@RequestParam (value = "id_doctor")long id,
												@RequestParam(value = "medical_facility_id")long mfid){
		return dds.ListJadwal(id, mfid);
	}
	
	@GetMapping("history/{id}")
	public List<Map<String, Object>> ListHistory(@PathVariable long id){
		return dds.ListHistory(id);
	}
	
	@GetMapping("pengalaman/{id}")
	public List<Map<String, Object>> Pengalaman(@PathVariable long id){
		return dds.Pengalaman(id);
	}
	
	@GetMapping("price/{id}")
	public List<Map<String, Object>> Price(@PathVariable long id){
		return dds.Price(id);
	}
	
	@GetMapping("chat")
	public List<Map<String, Object>> Chat(){
		return dds.Chat();
	}

}
