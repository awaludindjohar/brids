package com.reksa.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reksa.models.MenuModel;
import com.reksa.services.MenuService;

@RestController
@RequestMapping("apirole2")
public class RoleRestController {
	
	@Autowired
	private MenuService ms;
	
	
	@GetMapping("roles/{role_id}") 
	public List<Map<String, Object>> listId(@PathVariable long role_id,
										    HttpSession session) {
		String lRoleId = String.valueOf(session.getAttribute("lRoleId"));
		System.out.println(lRoleId);
		return ms.listId(role_id);
	}

}
