package com.reksa.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("bank")
public class BankController {
	
	@RequestMapping("/")
	public String bank() {
		return "/bank/Bank";
	}
	
	@RequestMapping("add")
	public String addbank() {
		return "/bank/AddBank";
	}
	
	@RequestMapping("upd")
	public String updbank() {
		return "/bank/UpdBank";
	}
	
	@RequestMapping("del")
	public String delbank() {
		return "/bank/DelBank";
	}

}
