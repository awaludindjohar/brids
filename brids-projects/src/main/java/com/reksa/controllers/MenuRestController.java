package com.reksa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reksa.models.BankModel;
import com.reksa.models.MenuModel;
import com.reksa.services.MenuService;

@RestController
@RequestMapping("apimenu")
public class MenuRestController {
	
	@Autowired
	private MenuService ms;
	
	@GetMapping("menu") 
	public List<MenuModel> menu() {
		return ms.menu();
	}
	
	@DeleteMapping("menu/{id}")
	public String delMenu(@PathVariable int id) {
		int hasil = ms.delMenu(id);
		if(hasil == 1) {
			return "Delete Sukses";
		} else {
			return "Delete Gagal";
		}
	}
	
	@PostMapping("menu")
	public String addMenu(@RequestBody MenuModel mm) {
		String nama = ms.cariNama(mm.getName());
		if(nama == null) {
			try {
				int hasil = ms.addMenu(mm.getName(), mm.getUrl(), mm.getParent_id(), mm.getBig_icon(), mm.getSmall_icon());
			} catch (Exception e) {
				return "Tambah Gagal, id Sudah di Pakai";
			}
		} else {
			return "Nama sudah di pakai";
		}
		return "Tambah Sukses";
	}

}
