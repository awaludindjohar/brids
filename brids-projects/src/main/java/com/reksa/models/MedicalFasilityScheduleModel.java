package com.reksa.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "m_medical_facility_schedule")
public class MedicalFasilityScheduleModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private long medical_facility_id;
	private String day;
	private String time_schedule_start;
	private String time_schedule_end;
	
	@Column(name = "createdBy")
    private Long createdBy;
	
    @Column(name = "createdOn")
    private LocalDateTime createdOn;
    
    @Column(name = "modifiedBy")
    private Long modifiedBy;
    
    @Column(name = "modifiedOn")
    private LocalDateTime modifiedOn;
    
    @Column(name = "deletedBy")
    private Long deletedBy;
    
    @Column(name = "deletedOn")
    private LocalDateTime deletedOn;
    
    @Column(name = "isDelete")
    private Boolean isDelete;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getMedical_facility_id() {
		return medical_facility_id;
	}

	public void setMedical_facility_id(long medical_facility_id) {
		this.medical_facility_id = medical_facility_id;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getTime_schedule_start() {
		return time_schedule_start;
	}

	public void setTime_schedule_start(String time_schedule_start) {
		this.time_schedule_start = time_schedule_start;
	}

	public String getTime_schedule_end() {
		return time_schedule_end;
	}

	public void setTime_schedule_end(String time_schedule_end) {
		this.time_schedule_end = time_schedule_end;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public LocalDateTime getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(LocalDateTime modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public LocalDateTime getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(LocalDateTime deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
    
    


}
