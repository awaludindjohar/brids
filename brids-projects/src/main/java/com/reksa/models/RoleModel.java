package com.reksa.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "m_role")
public class RoleModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	private String code;
	
	@Column(name = "createdBy")
    private Long createdBy;
	
    @Column(name = "createdOn")
    private LocalDateTime createdOn;
    
    @Column(name = "modifiedBy")
    private Long modifiedBy;
    
    @Column(name = "modifiedOn")
    private LocalDateTime modifiedOn;
    
    @Column(name = "deletedBy")
    private Long deletedBy;
    
    @Column(name = "deletedOn")
    private LocalDateTime deletedOn;
    
    @Column(name = "isDelete")
    private Boolean isDelete;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}	

}
