package com.reksa.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "m_user")
public class UserModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private long biodata_id;
	private long role_id;
	private String email;
	private String password;
	private int login_attempt;
	private Boolean is_locked;
	private LocalDateTime last_login;
	
	@Column(name = "createdBy")
    private Long createdBy;
	
    @Column(name = "createdOn")
    private LocalDateTime createdOn;
    
    @Column(name = "modifiedBy")
    private Long modifiedBy;
    
    @Column(name = "modifiedOn")
    private LocalDateTime modifiedOn;
    
    @Column(name = "deletedBy")
    private Long deletedBy;
    
    @Column(name = "deletedOn")
    private LocalDateTime deletedOn;
    
    @Column(name = "isDelete")
    private Boolean isDelete;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getBiodata_id() {
		return biodata_id;
	}
	public void setBiodata_id(long biodata_id) {
		this.biodata_id = biodata_id;
	}
	public long getRole_id() {
		return role_id;
	}
	public void setRole_id(long role_id) {
		this.role_id = role_id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getLogin_attempt() {
		return login_attempt;
	}
	public void setLogin_attempt(int login_attempt) {
		this.login_attempt = login_attempt;
	}
	public Boolean getIs_locked() {
		return is_locked;
	}
	public void setIs_locked(Boolean is_locked) {
		this.is_locked = is_locked;
	}
	public LocalDateTime getLast_login() {
		return last_login;
	}
	public void setLast_login(LocalDateTime last_login) {
		this.last_login = last_login;
	}	
	
}
