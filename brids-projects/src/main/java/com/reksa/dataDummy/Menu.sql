insert into m_menu(is_delete,created_by, created_on,name,url,parent_id,big_icon,small_icon) values
(false, 1 , now() , 'Bank'                    , 'Bank'                              , 1 , 'icofont-heart-beat' , 'master_small.jpg'       ),
(false, 1 , now() , 'Cari Dokter'           , 'caridokter'              , 2 , 'icofont-drug'       , 'transaksi_small.jpg'    ),
(false, 1 , now() , 'Atur Tindakan'           , 'localhost/aturtindakandokter'      , 2 , 'icofont-dna-alt-2'  , 'atd_small.jpg'          ),
(false, 2 , now() , 'Atur Harga Tindakan'     , 'localhost/aturhargatindakandokter' , 1 , 'icofont-heartbeat'  , 'ahtd_small.jpg'         ),
(false, 2 , now() , 'Kurir'                   , 'localhost/master/kurir'            , 3    , 'icofont-disabled'   , 'kurir_small.jpg'        ),
(false, 3 , now() , 'Spesialisasi'            , 'localhost/master/spesialisasi'     , 2    , 'icofont-autism'     , 'spesialisasi_small.jpg' ),
(false, 3 , now() , 'Buat Janji'              , 'localhost/transaksi/buatjanji'     , 3    , 'buatjanji_big.jpg'  , 'buatjanji_small.jpg'    ),

(false, 1 , now() , 'Menu 1' , '' , 1 , 'icofont-heart-beat' , 'menu1_small.jpg' ),
(false, 1 , now() , 'Menu 2' , '' , 2 , 'icofont-drug'       , 'menu2_small.jpg' ),
(false, 1 , now() , 'Menu 3' , '' , 2 , 'icofont-dna-alt-2'  , 'menu3_small.jpg' ),
(false, 1 , now() , 'Menu 4' , '' , 1 , 'icofont-heartbeat'  , 'menu4_small.jpg' ),
(false, 1 , now() , 'Menu 5' , '' , 3 , 'icofont-disabled'   , 'menu5_small.jpg' ),
(false, 1 , now() , 'Menu 6' , '' , 2 , 'icofont-autism'     , 'menu6_small.jpg' );

create or replace view vrole as 
select mu.email,mu.password,mmr.role_id,mb.fullname,mr.name,mr.code,mm.big_icon,mm.name as "NamaMenu" from m_menu mm
join m_menu_role mmr on mm.id = mmr.menu_id
join m_role mr on mr.id = mmr.role_id
join m_user mu on mu.role_id = mr.id
join m_biodata mb on mb.id = mu.biodata_id

