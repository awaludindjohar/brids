insert into m_doctor(biodata_id,str,created_by,created_on,is_delete) values
(5 , '123', 1, '2022-08-01 10:40:57.869675',false),
(6 , '321', 1, '2022-08-01 10:40:57.869675',false),
(7 , '312', 1, '2022-08-01 10:40:57.869675',false);

insert into m_specialization(name,created_by,created_on,is_delete) values
('Spesialis Anak', 1, '2022-08-01 10:40:57.869675',false),
('Spesialis Kulit', 1, '2022-08-01 10:40:57.869675',false),
('Spesialis Gigi', 1, '2022-08-01 10:40:57.869675',false);

insert into t_current_doctor_specialization(doctor_id,specialization_id,created_by,created_on,is_delete) values
(1,1, 1, '2022-08-01 10:40:57.869675',false),
(2,2, 1, '2022-08-01 10:40:57.869675',false),
(3,3, 1, '2022-08-01 10:40:57.869675',false);

insert into t_doctor_treatment(doctor_id,name,created_by,created_on,is_delete) values
(1 , 'Fisioterapi anak', 1, '2022-08-01 10:40:57.869675',false),
(1 , 'Konsultasi kesehatan anak', 1, '2022-08-01 10:40:57.869675',false),
(1 , 'Skrining tumbuh kembang anak', 1, '2022-08-01 10:40:57.869675',false),
(1 , 'Vaksin anak', 1, '2022-08-01 10:40:57.869675',false),
(1 , 'Konsultasi energi anak', 1, '2022-08-01 10:40:57.869675',false),
(1 , 'Konsultasi psikologi anak', 1, '2022-08-01 10:40:57.869675',false),
(1 , 'Test pendengaran OAE', 1, '2022-08-01 10:40:57.869675',false),
(1 , 'Chat', 1, '2022-08-01 10:40:57.869675',false);

insert into m_doctor_education(doctor_id,education_level_id,institution_name,major,start_year,end_year,is_last_education,created_by,created_on,is_delete) values
(1 , 1, 'Universitas Padjadjaran','Kedokteran Umum', '2007', '2011', false, 1, '2011-08-01 10:40:57.869675',false),
(1 , 2, 'Universitas Padjadjaran','Spesialis Anak', '2011', '2016', false, 1, '2016-08-01 10:40:57.869675',false);

insert into m_medical_facility_category(name,created_by,created_on,is_delete) values
('Poliklinik Anak', 1, '2022-08-01 10:40:57.869675',false),
('Poliklinik Kulit', 1, '2022-08-01 10:40:57.869675',false),
('Poliklinik Gigi', 1, '2022-08-01 10:40:57.869675',false);

insert into m_medical_facility(name, medical_facility_category_id, location_id,
full_address, email, phone_code, phone, fax,created_by,created_on,is_delete) values
('RS Mitra', 1, 8, 'Jl Kebenaran Kav 3S Kecamatan, Kota', 'admin@rsmitra.com', '021', '98765432', '16433', 1, '2019-08-01 10:40:57.869675',false),
('RSIA Bunda', 1, 6, 'Jl Kesabaran No 932 Kecamatan, Kota', 'admin@rsbunda.com', '021', '98765432', '16433', 1, '2018-08-01 10:40:57.869675',false),
('RSUP Hasan Sadikin', 1, 7, 'Jl Braga No 77 Kecamatan, Kota', 'admin@rshasansadikin.com', '021', '98765432', '16433', 1, '2016-08-01 10:40:57.869675',false);

insert into t_doctor_office(doctor_id, medical_facility_id, specialization,created_by,created_on,is_delete,deleted_by,deleted_on) values
(1,1,'Spesialis Anak', 1, '2019-08-01 10:40:57.869675',false,null ,null),
(1,2,'Spesialis Anak', 1, '2018-08-01 10:40:57.869675',false,null , null),
(1,3,'Spesialis Anak', 1, '2016-08-01 10:40:57.869675',true,1, '2018-08-01 10:40:57.869675');

insert into m_medical_facility_schedule(medical_facility_id, day, time_schedule_start, time_schedule_end,created_by,created_on,is_delete) values
(1,'Senin', '18:00', '21:00', 1, '2022-08-01 10:40:57.869675',false),
(1,'Selasa', '09:00', '12:00', 1, '2022-08-01 10:40:57.869675',false),
(2,'Senin', '09:00', '12:00', 1, '2022-08-01 10:40:57.869675',false),
(2,'Selasa', '18:00', '21:00', 1, '2022-08-01 10:40:57.869675',false);

insert into t_doctor_office_schedule(doctor_id, medical_facility_schedule_id, slot,created_by,created_on,is_delete) values
(1,1,5, 1, '2022-08-01 10:40:57.869675',false),
(1,2,5, 1, '2022-08-01 10:40:57.869675',false),
(1,3,5, 1, '2022-08-01 10:40:57.869675',false),
(1,4,5, 1, '2022-08-01 10:40:57.869675',false);

insert into t_doctor_office_treatment(doctor_office_id, doctor_treatment_id, created_by,created_on,is_delete) values
(1,1, 1, '2022-08-01 10:40:57.869675',false),
(1,2, 1, '2022-08-01 10:40:57.869675',false),
(1,3, 1, '2022-08-01 10:40:57.869675',false),
(1,4, 1, '2022-08-01 10:40:57.869675',false),
(1,5, 1, '2022-08-01 10:40:57.869675',false),
(1,6, 1, '2022-08-01 10:40:57.869675',false),
(1,7, 1, '2022-08-01 10:40:57.869675',false),
(2,1, 1, '2022-08-01 10:40:57.869675',false),
(2,2, 1, '2022-08-01 10:40:57.869675',false),
(2,3, 1, '2022-08-01 10:40:57.869675',false),
(2,4, 1, '2022-08-01 10:40:57.869675',false),
(2,5, 1, '2022-08-01 10:40:57.869675',false),
(2,6, 1, '2022-08-01 10:40:57.869675',false),
(2,7, 1, '2022-08-01 10:40:57.869675',false),
(3,1, 1, '2022-08-01 10:40:57.869675',false),
(3,2, 1, '2022-08-01 10:40:57.869675',false),
(3,3, 1, '2022-08-01 10:40:57.869675',false),
(3,4, 1, '2022-08-01 10:40:57.869675',false),
(3,5, 1, '2022-08-01 10:40:57.869675',false),
(3,6, 1, '2022-08-01 10:40:57.869675',false),
(3,7, 1, '2022-08-01 10:40:57.869675',false);

insert into t_doctor_office_treatment_price(doctor_office_treatment_id, price, price_start_from,price_until_from,created_by,created_on,is_delete) values
(1, 250000, 100000, 400000, 1, '2022-08-01 10:40:57.869675',false),
(2, 325000, 200000, 450000, 1, '2022-08-01 10:40:57.869675',false),
(3, 500000, 365000, 850000, 1, '2022-08-01 10:40:57.869675',false),
(4, 275000, 150000, 400000, 1, '2022-08-01 10:40:57.869675',false),
(5, 275000, 150000, 400000, 1, '2022-08-01 10:40:57.869675',false),
(6, 300000, 250000, 350000, 1, '2022-08-01 10:40:57.869675',false),
(7, 160000, 90000, 200000, 1, '2022-08-01 10:40:57.869675',false),
(8, 250000, 100000, 400000, 1, '2022-08-01 10:40:57.869675',false),
(9, 325000, 200000, 450000, 1, '2022-08-01 10:40:57.869675',false),
(10, 500000, 365000, 850000, 1, '2022-08-01 10:40:57.869675',false),
(11, 275000, 150000, 400000, 1, '2022-08-01 10:40:57.869675',false),
(12, 275000, 150000, 400000, 1, '2022-08-01 10:40:57.869675',false),
(13, 300000, 250000, 350000, 1, '2022-08-01 10:40:57.869675',false),
(14, 160000, 90000, 200000, 1, '2022-08-01 10:40:57.869675',false),
(15, 250000, 100000, 400000,1,  '2022-08-01 10:40:57.869675',false),
(16, 325000, 200000, 450000, 1, '2022-08-01 10:40:57.869675',false),
(17, 500000, 365000, 850000,1,  '2022-08-01 10:40:57.869675',false),
(18, 275000, 150000, 400000,1,  '2022-08-01 10:40:57.869675',false),
(19, 275000, 150000, 400000,1,  '2022-08-01 10:40:57.869675',false),
(20, 300000, 250000, 350000,1,  '2022-08-01 10:40:57.869675',false),
(21, 160000, 90000, 200000,1,  '2022-08-01 10:40:57.869675',false),
(22, 30000, 30000, 30000, 1,  '2022-08-01 10:40:57.869675',false);

Insert Into m_level_location (name, abbreviation) values
('Provinsi','Prov.'),
('Kota','Kota'),
('Kabupaten','Kab.'),
('Kecamatan','Kec.'),
('Kelurahan','Kel.');

Insert Into m_location (location_level_id,name, parent_id ) Values
(1,'Jakarta', 1),
(2,'Jakarta Selatan', 2),
(4,'Pasar Minggu', 3),
(5,'Kramat Pela',4 ),
(2,'Jakarta Pusat', 2),
(2,'Bandung', 3),
(2,'Depok', 1);

insert into m_education_level(name,is_delete,created_by,created_on) values
('S1',false,1,now()),
('S2',false,2,now()),
('S3',false,3,now());

create or replace view vdoctor as
select s.name as spesialis, b.fullname, b.image_path,
b.image, d.id as id_doctor
from m_biodata b join m_doctor d 
on b.id = d.biodata_id
join t_current_doctor_specialization ds
on d.id = ds.doctor_id
join m_specialization s
on s.id = ds.specialization_id;

create or replace view vtindakan as
select d.id as id_doctor, dt.name as tindakan
from m_doctor d join t_doctor_treatment dt
on d.id = dt.doctor_id;

create or replace view vpendidikan_dokter as
select de.id, el.name, de.end_year, de.institution_name, de.major, de.doctor_id as id_doctor
from m_doctor_education de
join m_education_level el
on de.education_level_id = el.id
order by de.end_year desc;

create or replace view vlokasi_praktek as
select tdo.id, tdo.doctor_id as id_doctor, mf.name, mf.full_address, mfc.name as kategori, tdo.is_delete,
mf.location_id, mf.medical_facility_category_id
from t_doctor_office tdo
join m_medical_facility mf
on tdo.medical_facility_id = mf.id
join m_medical_facility_category mfc
on mf.medical_facility_category_id = mfc.id;

create or replace view vschedule as
select dos.id, mfs.medical_facility_id , mfs.day, mfs.time_schedule_start,
mfs.time_schedule_end, dos.doctor_Id as id_doctor
from t_doctor_office_schedule dos
join m_medical_facility_schedule mfs
on dos.medical_facility_schedule_id = mfs.id;

create or replace view vhistory as
select s.name as spesialis, d.id as id_doctor, mf.name as rs_name,
l.name as location_name, DATE_PART('YEAR',tdo.created_on) as start_year,
DATE_PART('YEAR',tdo.deleted_on) as end_year
from m_biodata b join m_doctor d on b.id = d.biodata_id
join t_current_doctor_specialization ds on d.id = ds.doctor_id
join m_specialization s on s.id = ds.specialization_id
join t_doctor_office tdo on tdo.doctor_id = d.id
join m_medical_facility mf on mf.id = tdo.medical_facility_id
join m_location l on l.id = mf.location_id;

create or replace view vpengalaman as
select DATE_PART('YEAR',NOW())-DATE_PART('YEAR',created_on) as pengalaman,
doctor_id as id_doctor
from t_doctor_office order by created_on asc limit 1;

create or replace view vprice as
select min(dotp.price_start_from) as start_from, dot.doctor_office_id
from t_doctor_office_treatment_price dotp
join t_doctor_office_treatment dot
on dotp.doctor_office_treatment_id = dot.id
group by dot.doctor_office_id;

create or replace view vchat as
select min(dotp.price_start_from) as start_from, dot.doctor_office_id
from t_doctor_office_treatment_price dotp
join t_doctor_office_treatment dot
on dotp.doctor_office_treatment_id = dot.id
group by dot.doctor_office_id
having dot.doctor_office_id is null;