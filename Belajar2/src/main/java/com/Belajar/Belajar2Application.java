package com.Belajar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Belajar2Application {

	public static void main(String[] args) {
		SpringApplication.run(Belajar2Application.class, args);
	}

}
